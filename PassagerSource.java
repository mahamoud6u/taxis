/**
 * Simule l'arrivée de passagers demandant des courses 
 * à une compagnie de taxi.
 * Des passagers devraient apparaître à des intervalles aléatoires.
 * 
 * @author David J. Barnes and Michael Kölling
 * @version 2011.07.31
 */
public class PassagerSource
{
    private TaxiCompany company;

    /**
     * Constructor for objects of class PassengerSource.
     * Constructeur des objets de la classe PassagerSource.
     * @param company La compagnie utilisée. Ne doit pas être null.
     * @throws NullPointerException si company est null.
     */
    public PassagerSource(TaxiCompany company)
    {
        if(company == null) {
            throw new NullPointerException("company");
        }
        this.company = company;
    }

    /**
     * Fait que la source engendre un nouveau passager et
     * demande une course à la compagnie.
     * @return true si la demande réussit, false autrement.
     */
    public boolean demandeCourse()
    {
        // **** CODE A MODIFIER POUR LE DS ****
        return false;
    }

    /**
     * Crée un nouveau passager.
     * @return Le passager créé.
     */
    public Passager creePassager()
    {
        return new Passager(new Position(0, 0), new Position(10, 20));
    }
}
