import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import junit.framework.TestCase;

/**
 * The test class PassagerTest.
 *
 * @author  David J. Barnes and Michael Kölling
 * @version 2011.07.31
 */
public class PassagerTest extends TestCase
{
    /**
     * Default constructor for test class PassagerTest
     */
    public PassagerTest()
    {
    }

    /**
     * Sets up the test fixture.
     *
     * Called before every test case method.
     */
    @Before
    protected void setUp()
    {
    }

    /**
     * Tears down the test fixture.
     *
     * Called after every test case method.
     */
    @After
    protected void tearDown()
    {
    }

    /**
     * Teste la création élémentaire d'un passager.
     * Vérifie que les positions de prise en charge et de destination
     * ont été créées.
     */
    @Test
	public void testCreation()
	{
		Position prise_en_charge = new Position(0, 0);
		Position destination = new Position(1, 2);
		Passager passager = new Passager(prise_en_charge, destination);
		assertEquals(destination, passager.getDestination());
		assertEquals(prise_en_charge, passager.getPriseEnChargePosition());
	}
}
